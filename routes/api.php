<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('students', 'ApiController@getAllStudents');
Route::get('students/{id}','ApiController@getStudent');
Route::post('students','ApiController@createStudent');
Route::put('students/{id}','ApiController@updateStudent');
Route::post('students/{id}','ApiController@deleteStudent');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
